package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class D_OCRDocumentCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(D_OCRDocumentCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    D_OCRDocument command = new D_OCRDocument();
    if(parameters.get("sessionName") == null || parameters.get("sessionName").get() == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.get("TheFile") == null || parameters.get("TheFile").get() == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","TheFile"));
    }



    if(parameters.get("OutputFilePath") == null || parameters.get("OutputFilePath").get() == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","OutputFilePath"));
    }

    command.setSessions(sessionMap);
    if(parameters.get("sessionName") != null && parameters.get("sessionName").get() != null && !(parameters.get("sessionName").get() instanceof String)) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
    }
    if(parameters.get("TheFile") != null && parameters.get("TheFile").get() != null && !(parameters.get("TheFile").get() instanceof String)) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","TheFile", "String", parameters.get("TheFile").get().getClass().getSimpleName()));
    }
    if(parameters.get("OCRSettings") != null && parameters.get("OCRSettings").get() != null && !(parameters.get("OCRSettings").get() instanceof String)) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","OCRSettings", "String", parameters.get("OCRSettings").get().getClass().getSimpleName()));
    }
    if(parameters.get("OCRLanguageList") != null && parameters.get("OCRLanguageList").get() != null && !(parameters.get("OCRLanguageList").get() instanceof String)) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","OCRLanguageList", "String", parameters.get("OCRLanguageList").get().getClass().getSimpleName()));
    }
    if(parameters.get("OutputFilePath") != null && parameters.get("OutputFilePath").get() != null && !(parameters.get("OutputFilePath").get() instanceof String)) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","OutputFilePath", "String", parameters.get("OutputFilePath").get().getClass().getSimpleName()));
    }
    try {
      Optional<Value> result =  Optional.ofNullable(command.action(parameters.get("sessionName") != null ? (String)parameters.get("sessionName").get() : (String)null ,parameters.get("TheFile") != null ? (String)parameters.get("TheFile").get() : (String)null ,parameters.get("OCRSettings") != null ? (String)parameters.get("OCRSettings").get() : (String)null ,parameters.get("OCRLanguageList") != null ? (String)parameters.get("OCRLanguageList").get() : (String)null ,parameters.get("OutputFilePath") != null ? (String)parameters.get("OutputFilePath").get() : (String)null ));
      logger.traceExit(result);
      return result;
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
