package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class C_LabelDocumentCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(C_LabelDocumentCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    C_LabelDocument command = new C_LabelDocument();
    if(parameters.get("sessionName") == null || parameters.get("sessionName").get() == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.get("ClassifierID") == null || parameters.get("ClassifierID").get() == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","ClassifierID"));
    }

    if(parameters.get("LabelName") == null || parameters.get("LabelName").get() == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","LabelName"));
    }

    if(parameters.get("DocumentPath") == null || parameters.get("DocumentPath").get() == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","DocumentPath"));
    }

    if(parameters.get("PageNumber") == null || parameters.get("PageNumber").get() == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","PageNumber"));
    }

    command.setSessions(sessionMap);
    if(parameters.get("sessionName") != null && parameters.get("sessionName").get() != null && !(parameters.get("sessionName").get() instanceof String)) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
    }
    if(parameters.get("ClassifierID") != null && parameters.get("ClassifierID").get() != null && !(parameters.get("ClassifierID").get() instanceof String)) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","ClassifierID", "String", parameters.get("ClassifierID").get().getClass().getSimpleName()));
    }
    if(parameters.get("LabelName") != null && parameters.get("LabelName").get() != null && !(parameters.get("LabelName").get() instanceof String)) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","LabelName", "String", parameters.get("LabelName").get().getClass().getSimpleName()));
    }
    if(parameters.get("DocumentPath") != null && parameters.get("DocumentPath").get() != null && !(parameters.get("DocumentPath").get() instanceof String)) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","DocumentPath", "String", parameters.get("DocumentPath").get().getClass().getSimpleName()));
    }
    if(parameters.get("PageNumber") != null && parameters.get("PageNumber").get() != null && !(parameters.get("PageNumber").get() instanceof String)) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","PageNumber", "String", parameters.get("PageNumber").get().getClass().getSimpleName()));
    }
    try {
      Optional<Value> result =  Optional.ofNullable(command.action(parameters.get("sessionName") != null ? (String)parameters.get("sessionName").get() : (String)null ,parameters.get("ClassifierID") != null ? (String)parameters.get("ClassifierID").get() : (String)null ,parameters.get("LabelName") != null ? (String)parameters.get("LabelName").get() : (String)null ,parameters.get("DocumentPath") != null ? (String)parameters.get("DocumentPath").get() : (String)null ,parameters.get("PageNumber") != null ? (String)parameters.get("PageNumber").get() : (String)null ));
      logger.traceExit(result);
      return result;
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
