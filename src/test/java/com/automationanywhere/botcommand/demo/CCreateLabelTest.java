package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class CCreateLabelTest {

    C_CreateLabel command = new C_CreateLabel();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){
        return new Object[][]{
                {"http://localhost:11051","8369fac7-9666-4add-aacb-fc894a3f347f","PO3","ok"}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String URL, String ClassifierID, String LabelName, String Results){
        Value<String> d = command.action(URL, ClassifierID, LabelName);
        String myList = d.get();

        System.out.println("DEBUG RES:"+myList);
        //assertEquals(myList,Results );
        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();

    }
}
