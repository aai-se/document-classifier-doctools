package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class DGetBarCodesTest {

    D_GetBarCodes command = new D_GetBarCodes();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
               //{"http://localhost:11051","C:\\BBT\\unsorted_documents\\Invoice1.pdf","e85e3c7d-3e7b-4495-adb5-5671309a5f81","INVOICE","86.48"}
                {"Default","C:\\iqbot\\QRCodeExample.png",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String SessionName, String TheFile, String Res) throws Exception {
        BackendServer myBackendServ = new BackendServer("http://localhost:11051");
        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put("Default",myBackendServ);
        command.setSessions(mso);

        ListValue d = command.action("Default",TheFile);
        List<DictionaryValue> dictList = d.get();

        for(int i=0;i< dictList.size();i++){
            DictionaryValue dv = dictList.get(i);
            Value Val = dv.get().get("value");
            Value Page = dv.get().get("page");
            Value Type = dv.get().get("type");
            Value X = dv.get().get("x");
            Value Y = dv.get().get("y");
            Value Width = dv.get().get("width");
            Value Height = dv.get().get("height");

            System.out.println(Val.toString()+"|"+Page.toString()+"|"+Type.toString()+"|"+X.toString()+"|"+Y.toString()+"|"+Width.toString()+"|"+Height.toString());
        }


        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();

    }
}
