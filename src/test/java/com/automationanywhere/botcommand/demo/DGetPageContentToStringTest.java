package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Test(enabled=true)
public class DGetPageContentToStringTest {

    D_GetPageContentToString command = new D_GetPageContentToString();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Sorted\\Form1094-B\\1094-B-01.jpg","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1094-B,0.8773839621855956"},
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000858.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1094-C,0.8629813177788476"},
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000859.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1095-C,0.8185496945891348"},
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000860.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1095-C,0.6800148383921646"},
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000861.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1094-C,0.8753960921480336"},

                //{"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000921.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1095-C,0.7704248427240444"}

               //{"http://localhost:11051","C:\\BBT\\unsorted_documents\\Invoice1.pdf","e85e3c7d-3e7b-4495-adb5-5671309a5f81","INVOICE","86.48"}
                {"Default","096bf22f-3a2a-4506-b563-af46a0b89686",1.0,"|",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String URL, String DocID, Double PageNumber,String Separator, String Res){
        BackendServer myBackendServ = new BackendServer("http://localhost:11051");
        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put("Default",myBackendServ);
        command.setSessions(mso);
        //String a = "جمركي قبض ايصال";
        //System.out.println(a);

        StringValue d = command.action(URL,DocID,PageNumber,Separator);

        System.out.println("ALL:"+d.get());


        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();

    }
}
