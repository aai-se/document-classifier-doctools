package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class CCreateClassifierTest {

    C_CreateClassifier command = new C_CreateClassifier();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){
        return new Object[][]{
                {"http://localhost:11051",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String URL, String Results){
        Value<String> d = command.action(URL);
        String myList = d.get();

        System.out.println("DEBUG RES:"+myList);
        //assertEquals(myList,Results );
        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();

    }
}
