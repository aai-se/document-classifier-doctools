package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class CTrainClassifierTest {

    C_TrainClassifier command = new C_TrainClassifier();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){
        return new Object[][]{
                {"http://localhost:11051","073db739-c45d-4229-b92a-5b0b72ea65fb","Status: ok -> Training Successful"}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String URL, String ClassifierID, String Results){
        Value<String> d = command.action(URL, ClassifierID);
        String myList = d.get();

        System.out.println("DEBUG RES:"+myList);
        //assertEquals(myList,Results );
        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();

    }
}
