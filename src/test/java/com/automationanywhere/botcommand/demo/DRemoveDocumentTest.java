package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class DRemoveDocumentTest {

    D_RemoveDocument command = new D_RemoveDocument();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){
        return new Object[][]{
                {"http://localhost:11051","032f34b1-6591-40ca-b862-23985429e777","ok"}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String URL, String DocumentID, String Results){
        Value<String> d = command.action(URL,DocumentID);
        String myList = d.get();

        System.out.println("DEBUG RES:"+myList);
        //assertEquals(myList,Results );
        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();

    }
}
