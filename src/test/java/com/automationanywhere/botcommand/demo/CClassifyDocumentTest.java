package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class CClassifyDocumentTest {

    C_ClassifyDocument command = new C_ClassifyDocument();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Sorted\\Form1094-B\\1094-B-01.jpg","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1094-B,0.8773839621855956"},
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000858.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1094-C,0.8629813177788476"},
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000859.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1095-C,0.8185496945891348"},
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000860.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1095-C,0.6800148383921646"},
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000861.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1094-C,0.8753960921480336"},

                //{"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000921.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1095-C,0.7704248427240444"}

               //{"http://localhost:11051","C:\\BBT\\unsorted_documents\\Invoice1.pdf","e85e3c7d-3e7b-4495-adb5-5671309a5f81","INVOICE","86.48"}
                {"C:\\iqbot\\Document Samples\\Stock Documents\\Bank Statements\\Bank Statement - Ace Bank - 2.pdf","23bab163-afe2-4585-8ab1-e91d72539602","BANK STATEMENTS","96.65"}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String TheFile, String ClassifierID, String TheClass, String TheScore){
        BackendServer myBackendServ = new BackendServer("http://localhost:11051");
        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put("Default",myBackendServ);
        command.setSessions(mso);

        ListValue d = command.action("Default",TheFile, ClassifierID,"{\"deskewImage\":true}","[\"English\"]");
        List<DictionaryValue> dictList = d.get();

        System.out.println(dictList.size());
        for(int i=0;i< dictList.size();i++){
            DictionaryValue dv = dictList.get(i);
            //System.out.println(dv.get().toString());

            Map<String,Value> myPredMap = dv.get();
            Value predClass = myPredMap.get("class");
            System.out.println(predClass.toString());
            Value PredScore = myPredMap.get("score");
            System.out.println(PredScore.toString());
            Value PredPage = myPredMap.get("page");
            System.out.println(PredPage.toString());

            System.out.println(predClass.toString()+"|"+PredScore.toString()+"|"+PredPage.toString());
        }

        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();

    }
}
