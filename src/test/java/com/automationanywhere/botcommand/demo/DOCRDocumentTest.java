package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class DOCRDocumentTest {


    D_OCRDocument command = new D_OCRDocument();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
                {"Default","C:\\Users\\Administrator.EC2AMAZ-5L6MMDA\\Desktop\\EU\\2020-05-27_1242.png","{\"deskewImage\":true}","[\"French\"]","C:\\Users\\Administrator.EC2AMAZ-5L6MMDA\\Desktop\\EU\\2020-05-27_1242_PACKAGE.pdf",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String SessionName, String TheFile,String OCRParams,String LanguageList, String OutputFile, String Res){
        BackendServer myBackendServ = new BackendServer("http://localhost:11051");
        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put("Default",myBackendServ);
        command.setSessions(mso);

        BooleanValue d = command.action(SessionName,TheFile,OCRParams,LanguageList,OutputFile);
        Boolean res = d.get();
        Boolean expected = true;
        assertEquals(res,expected);

        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();

    }
}
