package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class DOCRDocumentToIDTest {


    D_OCRDocumentToID command = new D_OCRDocumentToID();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
                {"Default","C:\\iqbot\\Document Samples\\Arabic\\custom_3.pdf","{ \"deskewImage\":true, \"enhanceLocalContrast\":false, \"removeGarbage\":false, \"garbageSize\":-1, \"correct_resolution\":true, \"newResolution\":0, \"hslSaturationBoundaryToSuppress\":1, \"removeObjects\":false, \"colorToRemove\":2, \"objectsTypeToRemove\":2, \"correctDistortions\":false, \"removeNoise\":false, \"noiseModel\":0, \"textExtractMode\":false, \"aggressiveTextExtraction\":true }","[\"Arabic\"]",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String SessionName, String TheFile,String OCRParams,String LanguageList, String Res){
        BackendServer myBackendServ = new BackendServer("http://localhost:11051");
        Map<String,Object> mso = new HashMap<String,Object>();
        mso.put("Default",myBackendServ);
        command.setSessions(mso);

        StringValue d = command.action(SessionName,TheFile,OCRParams,LanguageList);
        System.out.println(d.get());

        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();

    }
}
