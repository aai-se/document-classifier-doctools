package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Map;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class DGetDocumentInfoTest {

    D_GetDocumentInfo command = new D_GetDocumentInfo();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Sorted\\Form1094-B\\1094-B-01.jpg","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1094-B,0.8773839621855956"},
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000858.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1094-C,0.8629813177788476"},
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000859.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1095-C,0.8185496945891348"},
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000860.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1095-C,0.6800148383921646"},
               // {"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000861.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1094-C,0.8753960921480336"},

                //{"http://localhost:11051","C:\\Program Files (x86)\\Automation Anywhere IQ Bot Extensions\\Doctools-Command-Line-Interface\\docs\\IRS-Sample-Forms\\Leidos-2019-12-18\\z0000921.tif","90b8e450-fa50-4e6d-bb74-a51c7dcca067","FORM1095-C,0.7704248427240444"}

               //{"http://localhost:11051","C:\\BBT\\unsorted_documents\\Invoice1.pdf","e85e3c7d-3e7b-4495-adb5-5671309a5f81","INVOICE","86.48"}
                {"http://localhost:11051","C:\\iqbot\\Document Samples\\Stock Documents\\Bank Statements\\Bank Statement - Ace Bank - 2.tiff",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String URL, String TheFile, String Res){
        DictionaryValue d = command.action(URL,TheFile,"{\"deskewImage\":true}","[\"French\"]");
        Map<String,Value> map = d.get();
        String k1 = map.get("id").toString();
        String k2 = map.get("size").toString();
        String k3 = map.get("pages").toString();
        String k4 = map.get("type").toString();
        String k5 = map.get("extension").toString();

        System.out.println("DEBUG RES:"+k1+":"+k2+":"+k3+":"+k4+":"+k5);

        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();

    }
}
