package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class CLabelDocumentTest {

    C_LabelDocument command = new C_LabelDocument();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){
        return new Object[][]{
                {"http://localhost:11051","7a399a2b-3be9-44e4-ac5c-9519da196c29","Invoice","C:\\Users\\demo\\Documents\\LabelledDocs\\PurchaseOrder\\PO AdvancedRobotics 15a.pdf","1","ok"}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String URL, String ClassifierID, String LabelName, String DocumentPath, String PageNumber, String Results){
        Value<String> d = command.action(URL, ClassifierID, LabelName, DocumentPath, PageNumber);
        String myList = d.get();

        System.out.println("DEBUG RES:"+myList);
        //assertEquals(myList,Results );
        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();c

    }
}
