package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.json.simple.parser.ParseException;
import sun.jvm.hotspot.debugger.Page;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.NUMBER;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(label="Docs - Get Page Content", name="Get Page Content", description="Get Page Content as List of Strings", icon="pkg.svg",
        node_label="Get Page",
        return_type= DataType.LIST, return_label="List of String extracted from Page", return_required=true)

public class D_GetPageContent {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    //private static final String DOCTOOLSURL = "http://localhost:11051";

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public ListValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            //@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = "URL to Backend Service", default_value_type = STRING) @NotEmpty String CLASSIFIERURL,
            @Idx(index = "2", type = AttributeType.TEXT) @Pkg(label = "Document ID", default_value_type = STRING) @NotEmpty String DocumentID,
            @Idx(index = "3", type = AttributeType.NUMBER) @Pkg(label = "Page Number", default_value_type = NUMBER) @NotEmpty Number PageNumber

    )
    {
        //if("".equals(CLASSIFIERURL)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "CLASSIFIERURL"));}
        if("".equals(DocumentID)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "DocumentID"));}
        if("".equals(PageNumber)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "PageNumber"));}

        BackendServer serv = (BackendServer) this.sessions.get(sessionName);
        String CLASSIFIERURL = serv.getURL();
        // Initiate the RestRequests packaged in a separa
        // te class
        RestRequests DocToolsRestRequests = new RestRequests(CLASSIFIERURL);

        ListValue myListValue = new ListValue();
        try {
            ArrayList<Value> ResList = DocToolsRestRequests.GetPageContent(DocumentID, PageNumber);
            myListValue.set(ResList);
            return myListValue;

        }catch(ParseException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }

    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
