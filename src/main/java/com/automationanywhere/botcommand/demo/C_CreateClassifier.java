package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 * @author Perry Leong
 *
 */
@BotCommand
//define package details
@CommandPkg(label="Class - Create Classifier", name="Create Classifier", description="Generates a new classifier and returns its ID", icon="pkg.svg",
        node_label="Create Classifier",
        return_type= STRING, return_label="Assign the output to variable", return_required=true)

public class C_CreateClassifier {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages"); //instantiate a messages class for errors

    @Sessions
    private Map<String, Object> sessions;

    //define input fields for packages
    @Execute
    public Value<String> action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName
        //@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = "URL to Classifier Service", default_value_type = STRING) @NotEmpty String ClassifierURL
    )
    {
        //throw exception if any variables are empty
       // if("".equals(ClassifierURL)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "ClassifierURL"));}

        BackendServer serv = (BackendServer) this.sessions.get(sessionName);
        String CLASSIFIERURL = serv.getURL();

        // Send a POST request to DocTools /classifiers endpoint to generate a new classifier
        CloseableHttpClient client0 = HttpClients.createDefault();

        HttpPost generateClassifier = new HttpPost(CLASSIFIERURL+"/classifiers"); // creates instance of HttpPost
        MultipartEntityBuilder builder = MultipartEntityBuilder.create(); //for advanced HTTP calls

        HttpEntity multipart = builder.build();
        generateClassifier.setEntity(multipart); //call HttpPost to upload file

        // Create global DocumentID variable to be used in later API calls
        String status = "";
        String responseMsg = "";
        try {
            CloseableHttpResponse response = client0.execute(generateClassifier); //call the API
            String responseXml = EntityUtils.toString(response.getEntity());
            EntityUtils.consume(response.getEntity()); //closes HTTP connection

            //response has different structure depending on "ok" or "error",  this handles both
            Map<String, Map<String, String>> javaRootMapObject = new Gson().fromJson(responseXml, Map.class);
            Map<String, String> javaRootMapObject2 = new Gson().fromJson(responseXml, Map.class);
            status = javaRootMapObject2.get("status");
            if(status.equals("error")) {
                responseMsg = javaRootMapObject2.get("message");
            }
            if(status.equals("ok")) {
                responseMsg = javaRootMapObject.get("result").get("response");
            }
            System.out.println(responseMsg);
        }
        catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }
        return new StringValue(responseMsg);
    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}

