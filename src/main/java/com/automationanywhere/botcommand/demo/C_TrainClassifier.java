package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//import java.net.http.HttpResponse;

/**
 * @author Perry Leong
 *
 */
@BotCommand
@CommandPkg(label="Class - Train Classifier", name="Train Classifier", description="Train a classifier based on the documents added to its labels. Returns success status.", icon="pkg.svg",
        node_label="Train Classifier",
        return_type= STRING, return_label="Assign the output to variable", return_required=false)

public class C_TrainClassifier {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public Value<String> action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            //@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = "Classifier URL", default_value_type = STRING) @NotEmpty String ClassifierURL,
            @Idx(index = "2", type = AttributeType.TEXT) @Pkg(label = "Classifier ID", default_value_type = STRING) @NotEmpty String ClassifierID
    )
    {
       // if("".equals(CLASSIFIERURL)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "ClassifierURL"));}
        if("".equals(ClassifierID)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "ClassifierID"));}

        BackendServer serv = (BackendServer) this.sessions.get(sessionName);
        String CLASSIFIERURL = serv.getURL();

        // Send a POST request to DocTools /documents endpoint to upload given file
        // Extract DocumentID from API response and use for following calls
        CloseableHttpClient client0 = HttpClients.createDefault();

        HttpPost trainClassifier = new HttpPost(CLASSIFIERURL+"/classifiers/"+ClassifierID+"/train"); // creates instance of HttpPost
        MultipartEntityBuilder builder = MultipartEntityBuilder.create(); //what is this?

        HttpEntity multipart = builder.build();
        trainClassifier.setEntity(multipart);

        // Create status response
        String status = "";
        String responseMsg = "";
        String statusMessage = "";
        try {
            CloseableHttpResponse response = client0.execute(trainClassifier);
            String responseXml = EntityUtils.toString(response.getEntity());
            EntityUtils.consume(response.getEntity());
            Map<String, Map<String, String>> javaRootMapObject = new Gson().fromJson(responseXml, Map.class);
            Map<String, String> javaRootMapObject2 = new Gson().fromJson(responseXml, Map.class);
            status = javaRootMapObject2.get("status");
            if(status.equals("error")) {
                responseMsg = javaRootMapObject2.get("message");
            }
            if(status.equals("ok")) {
                responseMsg = "Training Successful";
            }
            statusMessage = "Status: " + status + " -> " + responseMsg;
            System.out.println(statusMessage);
        }
        catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }
        return new StringValue(statusMessage);
    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}

