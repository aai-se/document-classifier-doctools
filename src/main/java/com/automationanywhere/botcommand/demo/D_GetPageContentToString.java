package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.NUMBER;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(label="Docs - Get Page Content to string", name="Get Page Content to string", description="Get Page Content as String", icon="pkg.svg",
        node_label="Get Page to string",
        return_type= STRING, return_label="String extracted from Page", return_required=true)

public class D_GetPageContentToString {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public StringValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "2", type = AttributeType.TEXT) @Pkg(label = "Document ID", default_value_type = STRING) @NotEmpty String DocumentID,
            @Idx(index = "3", type = AttributeType.NUMBER) @Pkg(label = "Page Number", default_value_type = NUMBER) @NotEmpty Number PageNumber,
            @Idx(index = "4", type = AttributeType.TEXT) @Pkg(label = "Separator", default_value_type = STRING, default_value = " ") @NotEmpty String Separator

    )
    {
        //if("".equals(CLASSIFIERURL)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "CLASSIFIERURL"));}
        if("".equals(DocumentID)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "DocumentID"));}
        if("".equals(PageNumber)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "PageNumber"));}

        BackendServer serv = (BackendServer) this.sessions.get(sessionName);
        String CLASSIFIERURL = serv.getURL();
        // Initiate the RestRequests packaged in a separa
        // te class
        RestRequests DocToolsRestRequests = new RestRequests(CLASSIFIERURL);
        
        ListValue myListValue = new ListValue();
        try {
            ArrayList<Value> ResList = DocToolsRestRequests.GetPageContent(DocumentID, PageNumber);
            myListValue.set(ResList);
            String AllContent = "";
            //String Separator = " ";
            for(Value v : ResList){
                //System.out.println(v);
                StringValue sv = (StringValue) v;
                if(AllContent.equals("")){AllContent = sv.get();}
                else{AllContent = AllContent + Separator + sv.get();}
            }

            return new StringValue(AllContent);

        }catch(ParseException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }

    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
