package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 */

@BotCommand
@CommandPkg(label="Docs - Get Barcodes", name="Get Barcodes", description="Get Barcodes", icon="pkg.svg",
        node_label="a Get Barcodes",
        return_type= DataType.LIST, return_sub_type = DataType.ANY,
        return_label="list of dictionaries with 7 keys: 'page', 'type', 'value','x', 'y', 'width', 'height'", return_required=true)

public class D_GetBarCodes {

    private static final Logger logger = LogManager.getLogger(D_GetBarCodes.class);

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    //private static final String DOCTOOLSURL = "http://localhost:11051";

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public ListValue<DictionaryValue> action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            //index = "1", type = AttributeType.TEXT) @Pkg(label = "URL to Backend Service", default_value_type = STRING) @NotEmpty String CLASSIFIERURL,
            @Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Input File Path", default_value_type = STRING) @NotEmpty String TheFile
    )
    {

        if("".equals(TheFile)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "InputFilePath"));}

        BackendServer serv = (BackendServer) sessions.get(sessionName);
        String CLASSIFIERURL = serv.getURL();

        ListValue AllBarCodes = new ListValue();

        // Initiate the RestRequests packaged in a separate class
        RestRequests DocToolsRestRequests = new RestRequests(CLASSIFIERURL);

        // Upload Document - Returns Document ID
        String DocumentID = "";

        DocumentID = DocToolsRestRequests.UploadDocument(TheFile);

        ArrayList<HashMap<String,Value>> AllBarCodesFromDoctools = new ArrayList<HashMap<String,Value>>();

        try {
            AllBarCodesFromDoctools = DocToolsRestRequests.GetBarcodes(DocumentID);
        }catch(ParseException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }

        ArrayList<DictionaryValue> AllValues = new ArrayList<DictionaryValue>();
        for(int i = 0;i<AllBarCodesFromDoctools.size();i++){
            DictionaryValue dv = new DictionaryValue();
            dv.set(AllBarCodesFromDoctools.get(i));
            AllValues.add(dv);
        }

        AllBarCodes.set(AllValues);
        return AllBarCodes;
    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
