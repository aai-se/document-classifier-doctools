package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import Utils.ClassifierUtils;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Julia Marcondes
 * Jan 14 2020: output changed to dictionary (Bren Sapience)
 * Mar 5 2020: Rewrite into more compact code (Bren Sapience)
 */

@BotCommand
@CommandPkg(label="Class - Classify Document", name="Classify Document", description="Returns highest classification results for a given document", icon="pkg.svg",
        node_label="Classify Document",
        return_type= DataType.LIST, return_sub_type = DataType.ANY, return_label="list of dictionaries with 3 keys: 'page', 'score' & 'class'", return_required=true)


public class C_ClassifyDocument {

    private static final Logger logger = LogManager.getLogger(C_ClassifyDocument.class);

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    //private static final String DOCTOOLSURL = "http://localhost:11051";
    private static final String KEYPAGE = "page";
    private static final String KEYCLASS = "class";
    private static final String KEYSCORE = "score";

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public ListValue<DictionaryValue> action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            //index = "1", type = AttributeType.TEXT) @Pkg(label = "URL to Backend Service", default_value_type = STRING) @NotEmpty String CLASSIFIERURL,
            @Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Input File Path", default_value_type = STRING) @NotEmpty String TheFile,
            @Idx(index = "3", type = AttributeType.TEXT) @Pkg(label = "Classifier ID", default_value_type = STRING) @NotEmpty String ClassifierID,
            @Idx(index = "4", type = AttributeType.TEXTAREA) @Pkg(label = "OCR Processing Settings", default_value_type = STRING,  default_value =  "{ \"deskewImage\":true, \"enhanceLocalContrast\":false, \"removeGarbage\":false, \"garbageSize\":-1, \"correct_resolution\":true, \"newResolution\":0, \"hslSaturationBoundaryToSuppress\":1, \"removeObjects\":false, \"colorToRemove\":2, \"objectsTypeToRemove\":2, \"correctDistortions\":false, \"removeNoise\":false, \"noiseModel\":0, \"textExtractMode\":false, \"aggressiveTextExtraction\":true }") String OCRSettings,
            @Idx(index = "5", type = AttributeType.TEXT) @Pkg(label = "OCR Language List", default_value_type = STRING,  default_value = "[\"English\"]") String OCRLanguageList

    )
    {
        //if("".equals(CLASSIFIERURL)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "CLASSIFIERURL"));}
        if("".equals(ClassifierID)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "ClassifierID"));}
        if("".equals(TheFile)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "InputFilePath"));}

        ListValue AllPredictionsList = new ListValue();

        ArrayList<DictionaryValue> AllPredsAsDictionaries = new ArrayList<DictionaryValue>();

        BackendServer serv = (BackendServer) this.sessions.get(sessionName);
        String CLASSIFIERURL = serv.getURL();

        // Initiate the RestRequests packaged in a separate class
        RestRequests DocToolsRestRequests = new RestRequests(CLASSIFIERURL);

        // Upload Document - Returns Document ID
        String DocumentID = "";

        DocumentID = DocToolsRestRequests.UploadDocument(TheFile);

        //System.out.println("DEBUG: "+DocumentID);

        // OCR Document
        try{
            boolean status = DocToolsRestRequests.OCRDocumentWithParams(DocumentID,OCRSettings,OCRLanguageList);
            if(!status) {
                return AllPredictionsList;
            }
        }catch(ParseException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }

        //System.out.println("HERE: ");
        ArrayList<ClassifierUtils.PagePrediction> AllPredictions = new ArrayList<ClassifierUtils.PagePrediction>();

        try {
            AllPredictions = DocToolsRestRequests.ClassifyDocument(DocumentID,ClassifierID);
        }catch(ParseException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }


        for(int i=0;i<AllPredictions.size();i++){
            Map<String,Value> ResMap = new LinkedHashMap();
            ResMap.put(KEYPAGE,new StringValue(Integer.toString(i+1)));
            ResMap.put(KEYSCORE,new StringValue("0"));
            ResMap.put(KEYCLASS,new StringValue(""));

            ClassifierUtils.PagePrediction pagePred = AllPredictions.get(i);
            ArrayList<ClassifierUtils.SinglePrediction> ClassPredictionsForPage = pagePred.allClassifierPredictions;
            if(ClassPredictionsForPage.size()>0){
                ClassifierUtils.SinglePrediction pred = ClassPredictionsForPage.get(0); //getting only the first prediction
                double PredScore = pred.getConfidence();
                String ReadablePercentagePrediction = ClassifierUtils.ConvertScoreToHumanReadable(PredScore);
                ResMap.replace(KEYSCORE,new StringValue(ReadablePercentagePrediction));
                ResMap.replace(KEYCLASS,new StringValue(pred.getLabel()));
            }
            DictionaryValue dv = new DictionaryValue();
            dv.set(ResMap);
            AllPredsAsDictionaries.add(dv);
        }
        AllPredictionsList.set(AllPredsAsDictionaries);
        return AllPredictionsList;


    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
