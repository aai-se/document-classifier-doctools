package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import Utils.ClassifierUtils;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.DICTIONARY;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(label="Docs - OCR Document to PDF", name="OCR Document to PDF", description="OCR a Document and return a Machine Readable version of it.", icon="pkg.svg",
        node_label="OCR Document to PDF",
        return_type= DataType.BOOLEAN, return_label="", return_required=true)

public class D_OCRDocument {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    //private static final String DOCTOOLSURL = "http://localhost:11051";

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public BooleanValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Input File Path", default_value_type = STRING) @NotEmpty String TheFile,
            @Idx(index = "3", type = AttributeType.TEXTAREA) @Pkg(label = "OCR Processing Settings", default_value_type = STRING,  default_value = "{ \"deskewImage\":true, \"enhanceLocalContrast\":false, \"removeGarbage\":false, \"garbageSize\":-1, \"correct_resolution\":true, \"newResolution\":0, \"hslSaturationBoundaryToSuppress\":1, \"removeObjects\":false, \"colorToRemove\":2, \"objectsTypeToRemove\":2, \"correctDistortions\":false, \"removeNoise\":false, \"noiseModel\":0, \"textExtractMode\":false, \"aggressiveTextExtraction\":true }") String OCRSettings,
            @Idx(index = "4", type = AttributeType.TEXT) @Pkg(label = "OCR Language List", default_value_type = STRING,  default_value = "[\"English\"]") String OCRLanguageList,
            @Idx(index = "5", type = AttributeType.FILE) @Pkg(label = "Output File Path", default_value_type = STRING) @NotEmpty String OutputFilePath
    )
    {


        //if("".equals(CLASSIFIERURL)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "CLASSIFIERURL"));}
        if("".equals(TheFile)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "InputFilePath"));}
        if("".equals(OutputFilePath)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "OutputFilePath"));}

        BackendServer serv = (BackendServer) this.sessions.get(sessionName);
        String CLASSIFIERURL = serv.getURL();

        // Initiate the RestRequests packaged in a separate class
        RestRequests DocToolsRestRequests = new RestRequests(CLASSIFIERURL);

        // Upload Document - Returns Document ID
        String DocumentID = "";
        DocumentID = DocToolsRestRequests.UploadDocument(TheFile);

        //System.out.println("DEBUG ID:"+DocumentID);

        // OCR Document
        try {
            boolean status = DocToolsRestRequests.OCRDocumentWithParams(DocumentID,OCRSettings,OCRLanguageList);
            //boolean status = DocToolsRestRequests.OCRDocumentWithParams(DocumentID,"[\"eng\"]","{\"cropImage\":false,\"correctOrientation\":true,\"deskewImage\":true}");
            if(!status){
                return new BooleanValue("false");
            }
        }catch(ParseException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }

        try {
            String Output = DocToolsRestRequests.DownloadDocument(DocumentID, OutputFilePath);
        }catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }

        return new BooleanValue("true");
    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
