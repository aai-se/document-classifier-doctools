package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.parser.ParseException;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 * @author Perry Leong
 *
 */
@BotCommand
@CommandPkg(label="Class - Label Document", name="Label Document", description="Creates a label associated with a specified classifier and adds a document to that label. Returns success status.", icon="pkg.svg",
        node_label="Label Document",
        return_type= STRING, return_label="Assign the output to variable", return_required=true)

public class C_LabelDocument {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    //private static final String DOCTOOLSURL = "http://localhost:11051";

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public Value<String> action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            //@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = "Classifier URL", default_value_type = STRING) @NotEmpty String ClassifierURL,
            @Idx(index = "2", type = AttributeType.TEXT) @Pkg(label = "Classifier ID", default_value_type = STRING) @NotEmpty String ClassifierID,
            @Idx(index = "3", type = AttributeType.TEXT) @Pkg(label = "Label Name", default_value_type = STRING) @NotEmpty String LabelName,
            @Idx(index = "4", type = AttributeType.TEXT) @Pkg(label = "Document Path", default_value_type = STRING) @NotEmpty String DocumentPath,
            @Idx(index = "5", type = AttributeType.TEXT) @Pkg(label = "Page Number", default_value_type = STRING) @NotEmpty String PageNumber
    )
    {
        //if("".equals(CLASSIFIERURL)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "ClassifierURL"));}
        if("".equals(ClassifierID)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "ClassifierID"));}
        if("".equals(LabelName)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "LabelName"));}
        if("".equals(DocumentPath)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "DocumentPath"));}
        if("".equals(PageNumber)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "PageNumber"));}

        BackendServer serv = (BackendServer) this.sessions.get(sessionName);
        String CLASSIFIERURL = serv.getURL();

        // Send a POST request to DocTools /documents endpoint to upload given file
        // Extract DocumentID from API response and use for following calls
        CloseableHttpClient client0 = HttpClients.createDefault();

        HttpPost uploadFile = new HttpPost(CLASSIFIERURL+"/documents"); // creates instance of HttpPost
        MultipartEntityBuilder builder = MultipartEntityBuilder.create(); //what is this?

        try {
            // This attaches TheFile to the POST:
            File document = new File(DocumentPath);
            builder.addBinaryBody(
                    "file",
                    new FileInputStream(document),
                    ContentType.APPLICATION_OCTET_STREAM,
                    document.getName()
            );
        }
    catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }

        HttpEntity multipart = builder.build();
        uploadFile.setEntity(multipart); //call HttpPost to upload file

        // Create global DocumentID variable to be used in later API calls
        String DocumentID = "";
        try {
            CloseableHttpResponse response = client0.execute(uploadFile); //new instance of uploadFile
            String responseXml = EntityUtils.toString(response.getEntity());
            EntityUtils.consume(response.getEntity());
            Map<String, Map<String, String>> javaRootMapObject = new Gson().fromJson(responseXml, Map.class);
            DocumentID = javaRootMapObject.get("result").get("response"); //assign response to DocumentID
        }
        catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }

        // Use /documents/:id endpoint to verify if document is a PDF
        // If not, use /ocr endpoint to convert it to PDF
        Boolean isPDF = true;
        try {
            CloseableHttpClient client1 = HttpClients.createDefault();
            HttpGet httpget = new HttpGet( CLASSIFIERURL + "/documents/" + DocumentID);
            CloseableHttpResponse response = client1.execute(httpget);
            String responseXml = EntityUtils.toString(response.getEntity());
            EntityUtils.consume(response.getEntity());
            Map<String, Map<String, Map<String, String>>> javaRootMapObject = new Gson().fromJson(responseXml, Map.class);
            System.out.println("DEBUG78: "+javaRootMapObject);
            System.out.println("DEBUG78: "+javaRootMapObject.get("result").get("response").get("contentExtension"));
            // Verify file extension in response
            String FileExtension = javaRootMapObject.get("result").get("response").get("contentExtension");

            if (FileExtension == null){ //to prevent nullpointerexception
                isPDF = false;
            }
            else if (!FileExtension.equals(".pdf")) {
                isPDF = false;
            }
        }
        catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }

        // Make call to /ocr endpoint to convert any non-PDF into a PDF
        if (!isPDF) {
            try {
                CloseableHttpClient client2 = HttpClients.createDefault();
                HttpEntity entity = MultipartEntityBuilder
                        .create()
                        .addTextBody("languages", "[\"eng\"]")
                        .build();
                HttpPost httpPost = new HttpPost(CLASSIFIERURL + "/documents/" + DocumentID + "/ocr");
                httpPost.setEntity(entity);
                CloseableHttpResponse response = client2.execute(httpPost);
                // Response does not need to be returned
                // PDF conversion is done in the background

                EntityUtils.consume(response.getEntity());
            }catch(IOException e){
                throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
            }
        }

        // Send a POST request to DocTools to add a document to a label

        CloseableHttpClient client3 = HttpClients.createDefault();

        String CompleteURL = CLASSIFIERURL + "/classifiers/" + ClassifierID + "/labels/" + LabelName + "/documents?document=" + DocumentID + "&pageNumber=" + PageNumber;
        HttpPost trainClassifier = new HttpPost(CompleteURL); // creates instance of HttpPost
        MultipartEntityBuilder builder1 = MultipartEntityBuilder.create(); //what is this?

        HttpEntity multipart1 = builder1.build();
        trainClassifier.setEntity(multipart1); //call HttpPost to upload file

        // Create status response
        String status = "";
        String responseMsg = "";
        String statusMessage = "";
        try {
            CloseableHttpResponse response = client3.execute(trainClassifier); //new instance of uploadFile
            String responseXml = EntityUtils.toString(response.getEntity());
            EntityUtils.consume(response.getEntity());

            Map<String, Map<String, String>> javaRootMapObject = new Gson().fromJson(responseXml, Map.class);
            Map<String, String> javaRootMapObject2 = new Gson().fromJson(responseXml, Map.class);
            status = javaRootMapObject2.get("status");
            if(status.equals("error")) {
                responseMsg = javaRootMapObject2.get("message");
            }
            if(status.equals("ok")) {
                responseMsg = javaRootMapObject.get("result").get("response");
            }
            statusMessage = "Status: " + status + " -> " + responseMsg;
            System.out.println(statusMessage);
        }
        catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }
        return new StringValue(DocumentID); //modified to return Document ID instead of label status
        //return new StringValue(statusMessage);
    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}

