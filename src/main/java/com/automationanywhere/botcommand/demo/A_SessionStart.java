package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Brendan Sapience
 */


@BotCommand
@CommandPkg(label = "Session Start", name = "SessionStart", description = "Session Start", icon = "", node_label = "session start {{sessionName}}|")
public class A_SessionStart {

    //private static final Logger logger = LogManager.getLogger(StartSession.class);

    @Sessions
    private Map<String, Object> sessions;

    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.demo.messages");

    @com.automationanywhere.commandsdk.annotations.GlobalSessionContext
    private com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext) {
        this.globalSessionContext = globalSessionContext;
    }

    @Execute
    public void start(@Idx(index = "1", type = TEXT) @Pkg(label = "Session name",  default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
                      @Idx(index = "2", type = TEXT) @Pkg(label = "Doctools Backend URL",  default_value_type = DataType.STRING, default_value = "http://localhost:11051") @NotEmpty String BackendURL
    ) throws Exception {

        // Check for existing session
        if (this.sessions.containsKey(sessionName)){
            throw new BotCommandException(MESSAGES.getString("Session name in use ")) ;
        }

        BackendServer myBackendServ = new BackendServer(BackendURL);
        this.sessions.put(sessionName, myBackendServ);

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }

}
