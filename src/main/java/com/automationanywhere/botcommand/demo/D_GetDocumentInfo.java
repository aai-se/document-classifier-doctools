package com.automationanywhere.botcommand.demo;

import Utils.BackendServer;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Bren Sapience
 */

@BotCommand
@CommandPkg(label="Docs - Get Document Info", name="Get Document Info", description="Get Information on Document", icon="pkg.svg",
        node_label="Get Info",
        return_type= DataType.DICTIONARY, return_label="Dictionary object with 5 keys: 'id','extension','type','pages','size'", return_required=true)

public class D_GetDocumentInfo {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    //private static final String DOCTOOLSURL = "http://localhost:11051";

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public DictionaryValue action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            //@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = "URL to Backend Service", default_value_type = STRING) @NotEmpty String CLASSIFIERURL,
            @Idx(index = "2", type = AttributeType.FILE) @Pkg(label = "Input File Path", default_value_type = STRING) @NotEmpty String TheFile,
            @Idx(index = "3", type = AttributeType.TEXTAREA) @Pkg(label = "OCR Processing Settings", default_value_type = STRING,  default_value = "{\"deskewImage\":true}") String OCRSettings,
            @Idx(index = "4", type = AttributeType.TEXT) @Pkg(label = "OCR Language List", default_value_type = STRING,  default_value = "[\"English\"]") String OCRLanguageList

    )
    {
       // if("".equals(CLASSIFIERURL)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "CLASSIFIERURL"));}
        if("".equals(TheFile)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "InputFilePath"));}

        BackendServer serv = (BackendServer) this.sessions.get(sessionName);
        String CLASSIFIERURL = serv.getURL();
        // Initiate the RestRequests packaged in a separate class
        RestRequests DocToolsRestRequests = new RestRequests(CLASSIFIERURL);

        // Upload Document - Returns Document ID
        String DocumentID = "";

        DocumentID = DocToolsRestRequests.UploadDocument(TheFile);

        try {
            boolean status = DocToolsRestRequests.OCRDocumentWithParams(DocumentID,OCRLanguageList,OCRSettings);
            if(!status){
                return new DictionaryValue();
            }
        } catch(ParseException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",e.getMessage())) ;
        }

        try {
            Map<String, Value> Output = DocToolsRestRequests.GetDocumentInfo(DocumentID);
            Output.put("id",new StringValue(DocumentID));
            return new DictionaryValue(Output);
        } catch (IOException | ParseException e) {
            return new DictionaryValue();
        }

    }
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
