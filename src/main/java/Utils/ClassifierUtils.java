package Utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class ClassifierUtils {

    public static class SinglePrediction{
        public double getConfidence() {
            return confidence;
        }

        public double confidence;

        public String getLabel() {
            return label;
        }

        public String label;

        public SinglePrediction(double conf,String predictedClass){
            this.confidence = conf;
            this.label = predictedClass;
        }

    }

    public static class PagePrediction{
        public long PageNumber;
        public double Similarity;

        public ArrayList<SinglePrediction> getAllClassifierPredictions() {
            return allClassifierPredictions;
        }

        public ArrayList<SinglePrediction> allClassifierPredictions;

        public PagePrediction(long pNumber,double simScore, ArrayList<SinglePrediction> preds){
            this.PageNumber = pNumber;
            this.Similarity = simScore;
            this.allClassifierPredictions = preds;
        }
    }

    public static String ConvertScoreToHumanReadable(double RawConfidenceScore){
        RawConfidenceScore = RawConfidenceScore * 100;
        double RoundedScore = round(RawConfidenceScore, 2);
        return Double.toString(RoundedScore);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
