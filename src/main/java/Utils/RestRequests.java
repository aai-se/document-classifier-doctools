package Utils;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class RestRequests {
    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    private String BACKENDURL = "";
    public RestRequests(String BackendURL){
        this.BACKENDURL = BackendURL;
    }

    //http://{{DOCTOOLSURL}}/documents/ba658fbf-9ff3-44fb-aa0a-7f54f5e45976/file
    public String DownloadDocument(String DocID, String OutputFilePath) throws IOException {

        File outputFile = new File(OutputFilePath);
        CloseableHttpClient client = HttpClients.createDefault();
        try (CloseableHttpResponse response = client.execute(new HttpGet(this.BACKENDURL + "/documents/" + DocID +"/file"))) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                try (FileOutputStream outstream = new FileOutputStream(outputFile)) {
                    entity.writeTo(outstream);
                }
            }
        }catch(Exception e){
            return "";
        }
        return OutputFilePath;
    }

    public ArrayList<ClassifierUtils.PagePrediction> ClassifyDocument(String DocID, String ClassifierID) throws IOException, ParseException {

        CloseableHttpClient client3 = HttpClients.createDefault();
        HttpEntity entity = MultipartEntityBuilder.create().addTextBody("document", DocID).build();
        String CompleteURL = this.BACKENDURL + "/classifiers/" + ClassifierID + "/classify";
        HttpPost httpPost = new HttpPost(CompleteURL);
        httpPost.setEntity(entity);
        CloseableHttpResponse response = client3.execute(httpPost);
        String JsonResponse = EntityUtils.toString(response.getEntity());

        //System.out.println(JsonResponse);
        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject) parse.parse(JsonResponse);

        // Check Status
        String status = jobj.get("status").toString();
        if(!status.equalsIgnoreCase("ok")){
            return null;
        }

        ArrayList<ClassifierUtils.PagePrediction> AllPredictions = RestResponse.ProcessClassifierResponse(jobj);

        return AllPredictions;
    }

    // passing params to OCR and Language or Language List
    public Boolean OCRDocumentWithParams(String DocID,String OCRSettings,String LanguageList) throws IOException, ParseException {

        // LanguageList = ["English"]
        // OCRSettings = {"cropImage": true,"correctOrientation":true}
        CloseableHttpClient client2 = HttpClients.createDefault();
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();

        builder.addTextBody("languages",LanguageList.trim());
        builder.addTextBody("settings",OCRSettings.trim());

        //builder.addTextBody("languages", "[\"English\"]");
        //builder.addTextBody("settings", "{\"deskewImage\":true}");

        HttpEntity entity = builder.build();
        //HttpEntity entity = MultipartEntityBuilder.create().addTextBody("languages", "[\"English\"]").addTextBody("settings", "{\"deskewImage\":true}").build();


        String URL = this.BACKENDURL + "/documents/" + DocID + "/ocr";

        HttpPost httpPost = new HttpPost(URL);
        httpPost.setEntity(entity);

        //System.out.println("URL: "+URL);

        CloseableHttpResponse response = client2.execute(httpPost);
        String JsonResponse = EntityUtils.toString(response.getEntity());
        System.out.println("RESP: "+JsonResponse);
        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject)parse.parse(JsonResponse);
        String oResult = (String)jobj.get("status");
        if(oResult.equalsIgnoreCase("ok")){
            return true;
        }else{
            return false;
        }
    }
/*
    public Boolean OCRDocument(String DocID) throws IOException, ParseException {

        CloseableHttpClient client2 = HttpClients.createDefault();
        HttpEntity entity = MultipartEntityBuilder.create().addTextBody("languages", "[\"English\"]").build();
        HttpPost httpPost = new HttpPost(this.BACKENDURL + "/documents/" + DocID + "/ocr");
        httpPost.setEntity(entity);
        CloseableHttpResponse response = client2.execute(httpPost);
        String JsonResponse = EntityUtils.toString(response.getEntity());
        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject)parse.parse(JsonResponse);
        String oResult = (String)jobj.get("status");
        if(oResult.equalsIgnoreCase("ok")){
            return true;
        }else{
            return false;
        }
    }
    */

/*
    public Boolean OCRDocumentLegacy(String DocID) throws IOException, ParseException {

        CloseableHttpClient client2 = HttpClients.createDefault();
        HttpEntity entity = MultipartEntityBuilder.create().addTextBody("languages", "[\"eng\"]").build();
        HttpPost httpPost = new HttpPost(this.BACKENDURL + "/documents/" + DocID + "/ocr");
        httpPost.setEntity(entity);
        CloseableHttpResponse response = client2.execute(httpPost);
        String JsonResponse = EntityUtils.toString(response.getEntity());
        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject)parse.parse(JsonResponse);
        String oResult = (String)jobj.get("status");
        if(oResult.equalsIgnoreCase("ok")){
            return true;
        }else{
            return false;
        }
    }
*/
    public  ArrayList<Value> GetPageContent(String DocID, Number PageNumber ) throws IOException, ParseException {

        CloseableHttpClient client1 = HttpClients.createDefault();
        String URL = this.BACKENDURL + "/documents/" + DocID + "/pages/"+PageNumber.intValue();
        //System.out.println(URL);
        HttpGet httpget = new HttpGet(URL);
        CloseableHttpResponse response = client1.execute(httpget);
        //HttpEntity ent = response.getEntity();
        //String JsonResponse = ent.toString();
        String JsonResponse = EntityUtils.toString(response.getEntity(),"UTF-8");
        //System.out.println(JsonResponse);

        ArrayList<Value> ContentList = new ArrayList<Value>();

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject)parse.parse(JsonResponse);
        JSONObject oResult = (JSONObject)jobj.get("result");
        //
        JSONObject oResponse = (JSONObject)oResult.get("response");
        JSONArray children = (JSONArray)oResponse.get("children");

        //String a = "جمركي قبض ايصال";

        for(int j=0;j<children.size();j++) {
            JSONObject aChild = (JSONObject) children.get(j);
            String OCRBlobValue = (String) aChild.get("value");
            ContentList.add(new StringValue(OCRBlobValue));
            //System.out.println(OCRBlobValue);
            //System.out.println("A:"+a);
        }
        return ContentList;

    }



    public  ArrayList<HashMap<String, Value>> GetBarcodes(String DocID) throws IOException, ParseException {

        CloseableHttpClient client1 = HttpClients.createDefault();
       // HttpEntity entity = MultipartEntityBuilder.create().addTextBody("languages", "[\"eng\"]").build();
        HttpPost httpPost = new HttpPost(this.BACKENDURL + "/documents/" + DocID+"/barcodes");
        //httpPost.setEntity(entity);

        CloseableHttpResponse response = client1.execute(httpPost);
        String JsonResponse = EntityUtils.toString(response.getEntity());

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject)parse.parse(JsonResponse);
        JSONObject oResult = (JSONObject)jobj.get("result");
        JSONArray allBarCodes = (JSONArray)oResult.get("response");

        ArrayList<HashMap<String, Value>> AllBarcodes = new ArrayList<HashMap<String,Value>>();

        for(int j=0;j<allBarCodes.size();j++) {
            HashMap<String, Value> BarcodeMap = new HashMap<String, Value>();

            JSONObject aBarcode = (JSONObject) allBarCodes.get(j);
            JSONObject aRegion = (JSONObject) aBarcode.get("region");
            Double X = (Double) aRegion.get("x");
            Double Y = (Double) aRegion.get("y");
            Double Width = (Double) aRegion.get("width");
            Double Height = (Double) aRegion.get("height");

            Long BarcodePage = (Long) aBarcode.get("page");
            String BarcodeType = (String) aBarcode.get("name");
            String BarcodeValue = (String) aBarcode.get("value");
            BarcodeMap.put("page", new StringValue(BarcodePage.toString()));
            BarcodeMap.put("type", new StringValue(BarcodeType));
            BarcodeMap.put("value", new StringValue(BarcodeValue));

            BarcodeMap.put("x", new StringValue(X.toString()));
            BarcodeMap.put("y", new StringValue(Y.toString()));
            BarcodeMap.put("width", new StringValue(Width.toString()));
            BarcodeMap.put("height", new StringValue(Height.toString()));

            AllBarcodes.add(BarcodeMap);

        }
        return AllBarcodes;
    }

    public  Map<String, Value> GetDocumentInfo(String DocID) throws IOException, ParseException {

        CloseableHttpClient client1 = HttpClients.createDefault();
        HttpGet httpget = new HttpGet(this.BACKENDURL + "/documents/" + DocID);
        CloseableHttpResponse response = client1.execute(httpget);
        String JsonResponse = EntityUtils.toString(response.getEntity());

        JSONParser parse = new JSONParser();
        JSONObject jobj = (JSONObject)parse.parse(JsonResponse);
        JSONObject oResult = (JSONObject)jobj.get("result");
        JSONObject oResponse = (JSONObject)oResult.get("response");
        String fileExtension = (String)oResponse.get("contentExtension");
        String fileType = (String)oResponse.get("contentType");
        Long PageCount = (Long)oResponse.get("pages");
        Long FileSize = (Long)oResponse.get("size");

        Map<String, Value> ResMap = new LinkedHashMap();
        ResMap.put("extension",new StringValue(fileExtension));
        ResMap.put("type",new StringValue(fileType));
        ResMap.put("pages",new NumberValue(PageCount.toString()));
        ResMap.put("size",new NumberValue(FileSize.toString()));

        return ResMap;
    }


    public String UploadDocument(String FilePath){
        // Send a POST request to DocTools /documents endpoint to upload given file
        // Extract DocumentID from API response and use for following calls
        CloseableHttpClient client0 = HttpClients.createDefault();

        HttpPost uploadFile = new HttpPost(this.BACKENDURL+"/documents");
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();

        // This attaches TheFile to the POST:
        try{
            File f = new File(FilePath);
            builder.addBinaryBody("file", new FileInputStream(f), ContentType.APPLICATION_OCTET_STREAM, f.getName());
        }catch(FileNotFoundException e){
            throw new BotCommandException(MESSAGES.getString("inputFileNotFound",FilePath)) ;
        }


        HttpEntity multipart = builder.build();
        uploadFile.setEntity(multipart);

        // Doctools GUID representing the document uploaded
        String DocumentID = "";
        CloseableHttpResponse response = null;
        try{
            response = client0.execute(uploadFile);
        }catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("errorDuringFileUpload",FilePath)) ;
        }

        String JsonResponse =  "";
        try{
            JsonResponse = EntityUtils.toString(response.getEntity());
        }catch(IOException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",response.getEntity())) ;
        }

        JSONParser parse = new JSONParser();
        try{
            JSONObject jobj = (JSONObject)parse.parse(JsonResponse);

            JSONObject oResult = (JSONObject)jobj.get("result");
            String DocID = (String)oResult.get("response");
            return DocID;
        }catch(ParseException e){
            throw new BotCommandException(MESSAGES.getString("jsonParsingError",JsonResponse)) ;
        }
    }
}
