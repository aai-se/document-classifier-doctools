package Utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;

public class RestResponse{


    public static ArrayList<ClassifierUtils.PagePrediction> ProcessClassifierResponse(JSONObject jobj){
        ArrayList<ClassifierUtils.PagePrediction> AllPagePredictions = new ArrayList<ClassifierUtils.PagePrediction>();

        JSONObject JsonResult = (JSONObject) jobj.get("result");
        JSONArray JSonResponse = (JSONArray) JsonResult.get("response");

        for(int i=0;i<JSonResponse.size();i++){
            JSONObject JSonClassificationsResp = (JSONObject) JSonResponse.get(i);
            long PageNumber = (long) JSonClassificationsResp.get("pageNumber");
            double Similarity = (double) JSonClassificationsResp.get("similarity");

            JSONArray JSonClassificationArray = (JSONArray) JSonClassificationsResp.get("classifications");
            ArrayList<ClassifierUtils.SinglePrediction> AllPredictions = new ArrayList<ClassifierUtils.SinglePrediction>();
            for(int j=0;j<JSonClassificationArray.size();j++){
                JSONObject JSonPrediction = (JSONObject) JSonClassificationArray.get(j);
                String Label = (String) JSonPrediction.get("label");
                double confScore = (double) JSonPrediction.get("confidence");
                ClassifierUtils.SinglePrediction pred = new ClassifierUtils.SinglePrediction(confScore,Label);
                AllPredictions.add(pred);
            }
            ClassifierUtils.PagePrediction pagepred = new ClassifierUtils.PagePrediction(PageNumber,Similarity,AllPredictions);
            AllPagePredictions.add(pagepred);
        }
        return AllPagePredictions;
    }


}